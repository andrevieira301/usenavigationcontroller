//
//  ViewController.m
//  UseNavigationController
//
//  Created by Andre Vieira on 17/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)nextViewControllerAction:(id)sender{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NextViewController *vc = [sb instantiateViewControllerWithIdentifier:@"NextViewController"];
    vc.textLabel = @"Next ViewController";
    [self.navigationController pushViewController:vc animated:YES];
}

@end

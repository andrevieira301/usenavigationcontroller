//
//  NextViewController.m
//  UseNavigationController
//
//  Created by Andre Vieira on 17/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "NextViewController.h"

@interface NextViewController ()

@property(weak,nonatomic)IBOutlet UILabel *label;

@end

@implementation NextViewController

-(void)viewDidLoad{
    self.label.text = self.textLabel;
}

@end

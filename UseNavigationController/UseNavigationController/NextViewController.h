//
//  NextViewController.h
//  UseNavigationController
//
//  Created by Andre Vieira on 17/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NextViewController : UIViewController

@property(strong,nonatomic)NSString *textLabel;

@end
